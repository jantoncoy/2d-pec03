/// <summary>
/// Contiene variables estaticas que sirven para ejecucion del juego 
/// </summary>
public static class OpcionesJuego
{
    /// <summary>
    /// Indica el numero de jugadores al cargar la escena
    /// </summary>
    static public int numeroJugadores = 1;

    /// <summary>
    /// Indica si es android el dispositivo de juego
    /// </summary>
    static public bool isAndroid = false;
}
