using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Contiene las opciones de las pulsaciones en el menu
/// </summary>
public class OpcionesMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

        //Identificamos si es la compilacion de android
        #if UNITY_ANDROID
                OpcionesJuego.isAndroid = true;
        #else
                OpcionesJuego.isAndroid = false;
        #endif
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Carga el modo un jugador
    /// </summary>
    public void jugar1p()
    {
        OpcionesJuego.numeroJugadores = 1;
        cargarScena();
    }

    /// <summary>
    /// Carga el modo dos jugadores
    /// </summary>
    public void jugar2p()
    {
        OpcionesJuego.numeroJugadores = 2;
        cargarScena();
    }

    /// <summary>
    /// Carga el juego
    /// </summary>
    private void cargarScena()
    {
        SceneManager.LoadScene("Juego");
    }

    /// <summary>
    /// Carga el menu
    /// </summary>
    public void cargarMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    /// <summary>
    /// Sale del juego si deja el mismo
    /// </summary>
    public void salir()
    {
        Application.Quit(1);
    }
}
