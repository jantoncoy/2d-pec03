﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{
    /// <summary>
    /// Dispara
    /// </summary>
    [Action("Acciones/Disparar")]
    [Help("Dispara el arma")]
    public class Disparar : GOAction
    {
        ///<value>Posicion del jugador</value>
        [InParam("jugador")]
        [Help("Posicion del jugador")]
        public Vector3 jugador;

        /// <summary>
        /// Ejecucion del servicio
        /// </summary>
        public override void OnStart()
        {
            Controlador controlador = this.gameObject.GetComponent<Controlador>();
            
            if (this.gameObject.transform.position.x < jugador.x)
            {
                if (!controlador.haciaDerecha)
                {
                    controlador.cambiarRotacion();
                }
            }
            else
            {
                if (controlador.haciaDerecha)
                {
                    controlador.cambiarRotacion();
                }
            }

            controlador.space = true;
        }

        /// <summary>
        /// Devolucion del servicio
        /// </summary>
        /// <returns></returns>
        public override TaskStatus OnUpdate()
        {
          return TaskStatus.COMPLETED;
        }
    }
}
