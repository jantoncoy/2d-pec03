﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{
    /// <summary>
    /// Se aleja de un jugador
    /// </summary>
    [Action("Navigation/AlejarseJugador")]
    [Help("Se aleja de un jugador")]
    public class AlejarseJugador : GOAction
    {
        ///<value>Posicion del jugador</value>
        [InParam("jugador")]
        [Help("Posicion del jugador")]
        public Vector3 jugador;

        private bool haciaDerecha = false;

        /// <summary>
        /// Ejecucion
        /// </summary>
        public override void OnStart()
        {
            Controlador controlador = this.gameObject.GetComponent<Controlador>();

            if (controlador.colisionaLimite)
            {
                controlador.colisionaLimite = false;
                haciaDerecha = false;
            }

            if (haciaDerecha)
            {
                //ir hacia la derecha
                controlador.right = true;
                controlador.up = true;
            }
            else
            {
                //ir hacia la izquierda
                controlador.left = true;
                controlador.up = true;
            }
        }

        /// <summary>
        /// Devolucion
        /// </summary>
        /// <returns></returns>
        public override TaskStatus OnUpdate()
        {
                return TaskStatus.COMPLETED;
        }
    }
}