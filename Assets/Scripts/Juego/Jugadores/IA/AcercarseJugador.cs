﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{
    /// <summary>
    /// Se acerca a un jugador
    /// </summary>
    [Action("Navigation/AcercarseJugador")]
    [Help("Se aleja de un jugador")]
    public class AcercarseJugador : GOAction
    {
        ///<value>Posicion del jugador</value>
        [InParam("jugador")]
        [Help("Posicion del jugador")]
        public Vector3 jugador;

        /// <summary>
        /// Ejecucion
        /// </summary>
        public override void OnStart()
        {
            Controlador controlador = this.gameObject.GetComponent<Controlador>();
            if (this.gameObject.transform.position.x < jugador.x)
            {
                //ir hacia la derecha
                controlador.right = true;
                controlador.up = true;
            }
            else
            {
                //ir hacia la izquierda
                controlador.left = true;
                controlador.up = true;
            }
        }

        /// <summary>
        /// Devolucion del servicio
        /// </summary>
        /// <returns></returns>
        public override TaskStatus OnUpdate()
        {
          return TaskStatus.COMPLETED;
        }
    }
}