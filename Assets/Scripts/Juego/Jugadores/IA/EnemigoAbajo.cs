﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{
    /// <summary>
    /// Comprueba si existe un enemigo abajo
    /// </summary>
    [Action("Sentidos/EnemigoAbajo")]
    [Help("Comprueba si existe un enemigo abajo")]
    public class EnemigoAbajo : GOAction
    {
        /// <summary>
        /// Determina si el enemigo esta abajo
        /// </summary>
        private bool abajo = false;

        /// <summary>
        /// Comprueba si existe un enemigo abajo
        /// </summary>
        /// <returns>Retorna true si el enemigo esta abajo</returns>
        public override void OnStart()
        {
            abajo = false;

            //solicitamos array de enemigos
            GameObject[] enemigos = EventosIA.obtenerJugadores(this.gameObject).ToArray();
            for (int a = 0; a < enemigos.Length; a++)
            {
                float distancia = Vector2.Distance(enemigos[a].transform.position, this.gameObject.transform.position);
                bool abajo = enemigos[a].transform.position.y < this.gameObject.transform.position.y;
                Debug.Log(distancia + " - " + abajo);
                if (distancia <= 0.08 && abajo)
                {
                    abajo = true;
                }
            }
        }

        /// <summary>
        /// Devolucion
        /// </summary>
        /// <returns></returns>
        public override TaskStatus OnUpdate()
        {
            if (abajo)
            {
                return TaskStatus.COMPLETED;
            }

            return TaskStatus.FAILED;
        }
    }
}
