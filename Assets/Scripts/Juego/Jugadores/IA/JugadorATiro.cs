using System.Collections.Generic;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Conditions
{
    /// <summary>
    /// Comprobamos si el jugador esta a tiro
    /// </summary>
    [Condition("Perception/JugadorATiro")]
    [Help("Comprobamos si el jugador esta a tiro")]
    public class JugadorATiro : GOCondition
    {
        ///<value>Distancia minima para considerar que un enemigo esta a tiro</value>
        [InParam("distanciaMinTiro")]
        [Help("Distancia minima para considerar que un enemigo esta a tiro")]
        public float distanciaMinTiro;

        ///<value>Distancia maxima para considerar que un enemigo esta a tiro</value>
        [InParam("distanciaMaxTiro")]
        [Help("Distancia maxima para considerar que un enemigo esta a tiro")]
        public float distanciaMaxTiro;

        ///<value>Vector del enemigo</value>
        [OutParam("vectorEnemigo")]
        [Help("Vector del enemigo")]
        public Vector3 vectorEnemigo;

        /// <summary>
        /// Calcula si el enemigo esta cerca
        /// </summary>
        /// <returns>Retorna true si el enemigo esta cerca</returns>
        public override bool Check()
        {
            //solicitamos array de enemigos
            GameObject[] enemigos = EventosIA.obtenerJugadores(this.gameObject).ToArray();
            for (int a = 0; a < enemigos.Length; a++)
            {
                float distancia = Vector2.Distance(enemigos[a].transform.position, this.gameObject.transform.position);
                if (distancia >= distanciaMinTiro && distancia <= distanciaMaxTiro)
                {
                    vectorEnemigo = enemigos[a].transform.position;
                    return true;
                }
            }

            return false;
        }
    }
}