using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{
    /// <summary>
    /// Comprueba si existe un enemigo cerca
    /// </summary>
    [Action("Sentidos/EnemigoCerca")]
    [Help("Comprueba si existe un enemigo cerca")]
    public class EnemigoCerca : GOAction
    {
        ///<value>Distancia maxima para considerar que un enemigo esta cerca</value>
        [InParam("distanciaMaxima")]
        [Help("Distancia maxima para considerar que un enemigo esta cerca")]
        public float distanciaMaxima;

        ///<value>Vector del enemigo</value>
        [OutParam("vectorEnemigo")]
        [Help("Vector del enemigo")]
        public Vector3 vectorEnemigo;

        /// <summary>
        /// Determina si el enemigo esta cerca
        /// </summary>
        private bool cerca = false;

        /// <summary>
        /// Calcula si el enemigo esta cerca
        /// </summary>
        /// <returns>Retorna true si el enemigo esta cerca</returns>
        public override void OnStart()
        {
            cerca = false;
            //solicitamos array de enemigos
            GameObject[] enemigos = EventosIA.obtenerJugadores(this.gameObject).ToArray();
            for(int a=0; a < enemigos.Length; a++)
            {
                float distancia = Vector2.Distance(enemigos[a].transform.position, this.gameObject.transform.position);
                if(distancia <= distanciaMaxima)
                {
                    vectorEnemigo = enemigos[a].transform.position;
                    cerca = true;
                    return;
                }
            }
        }

        /// <summary>
        /// Devolucion
        /// </summary>
        /// <returns></returns>
        public override TaskStatus OnUpdate()
        {
            if (cerca)
            {
                return TaskStatus.COMPLETED;
            }
 
            return TaskStatus.FAILED;
        }
    }
}