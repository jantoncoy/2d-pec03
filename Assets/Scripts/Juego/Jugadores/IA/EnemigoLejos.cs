using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{
    /// <summary>
    /// Comprueba si existe un enemigo lejos
    /// </summary>
    [Action("Sentidos/EnemigoLejos")]
    [Help("Comprueba si existe un enemigo lejos")]
    public class EnemigoLejos : GOAction
    {
        ///<value>Distancia minima para considerar que un enemigo esta lejos</value>
        [InParam("distanciaMinima")]
        [Help("Distancia minimo para considerar que un enemigo esta lejos")]
        public float distanciaMinima;

        ///<value>Vector del enemigo</value>
        [OutParam("vectorEnemigo")]
        [Help("Vector del enemigo")]
        public Vector3 vectorEnemigo;

        /// <summary>
        /// Determina si el enemigo esta lejos
        /// </summary>
        private bool lejos = false;

        /// <summary>
        /// Calcula si el enemigo esta lejos
        /// </summary>
        /// <returns>Retorna true si el enemigo esta lejos</returns>
        public override void OnStart()
        {
            lejos = false;
            //solicitamos array de enemigos
            GameObject[] enemigos = EventosIA.obtenerJugadores(this.gameObject).ToArray();
            for(int a=0; a < enemigos.Length; a++)
            {
                float distancia = Vector2.Distance(enemigos[a].transform.position, this.gameObject.transform.position);
                if(distancia > distanciaMinima)
                {
                    vectorEnemigo = enemigos[a].transform.position;
                    lejos = true;
                    return;
                }
            }
        }

        /// <summary>
        /// Devolucion
        /// </summary>
        /// <returns></returns>
        public override TaskStatus OnUpdate()
        {
            if (lejos)
            {
                return TaskStatus.COMPLETED;
            }

            return TaskStatus.FAILED;
        }
    }
}