using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Contiene las propiedades de los marcadores
/// </summary>
public class Marcadores : MonoBehaviour
{

    /// <summary>
    /// texto con marcador de nombre
    /// </summary>
    public TextMeshPro nombre;

    /// <summary>
    /// texto con marcador de vida
    /// </summary>
    public TextMeshPro vida;

    /// <summary>
    /// texto con marcador del escudo
    /// </summary>
    public TextMeshPro escudo;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
