using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Contiene la logica de los jugadores y de los npcs 
/// </summary>
public class Controlador : MonoBehaviour
{
    /// <summary>
    /// Vida del personaje
    /// </summary>
    public int vida = 100;

    /// <summary>
    /// Escudo del personaje
    /// </summary>
    public int escudo = 100;

    /// <summary>
    /// nombre del personaje
    /// </summary>
    public string nombre = "Jugador";

    /// <summary>
    /// Indica si funciona el jugador
    /// </summary>
    public bool habilitado = false;

    /// <summary>
    /// Indica si esta controlador por el pc
    /// </summary>
    public bool isIA = false;

    /// <summary>
    /// elemento de fisicas
    /// </summary>
    Rigidbody2D rigidBody = null;

    /// <summary>
    /// elemento de animaciones
    /// </summary>
    Animator animator = null;

    /// <summary>
    /// Indica si mira hacia la derecha
    /// </summary>
    public bool haciaDerecha = true;

    /// <summary>
    /// Botones de movimiento y ataque
    /// </summary>
    public bool left, right, up, down, space;

    /// <summary>
    /// Contiene el prefab de la bala para disparar
    /// </summary>
    public GameObject bala,traza;

    /// <summary>
    /// Marcador del nombre
    /// </summary>
    public TextMeshPro nombreUI;

    /// <summary>
    /// Marcador de vida
    /// </summary>
    public TextMeshPro vidaUI;

    /// <summary>
    /// Marcador de escudo
    /// </summary>
    public TextMeshPro escudoUI;

    /// <summary>
    /// Contiene la instancia del escudo
    /// </summary>
    public GameObject escudoObject;

    /// <summary>
    /// Marcadores
    /// </summary>
    public GameObject marcadores;

    /// <summary>
    /// Nos indica si colisionamos
    /// </summary>
    public bool colisionaLimite = false;

    /// <summary>
    /// colision con otro jugador
    /// </summary>
    public bool colisionaJugador = false;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = this.GetComponent<Rigidbody2D>();
        animator = this.GetComponent<Animator>();
        animator.SetInteger("Estado", 0);
        marcadores = Instantiate(marcadores,new Vector3(this.transform.position.x, this.transform.position.y+0.2f, this.transform.position.z),this.transform.rotation);
        nombreUI = marcadores.GetComponent<Marcadores>().nombre;
        vidaUI = marcadores.GetComponent<Marcadores>().vida;
        escudoUI = marcadores.GetComponent<Marcadores>().escudo;
        if (!isIA)
        {
            Destroy(this.GetComponent<BehaviorExecutor>());
        }
    }

    // Update is called once per frame
    void Update()
    {
 
        actualizarMarcadores();
        if (habilitado)
        {
            if (isIA)
            {
                controlar();
                resetearControles();
            }
            else
            {
                actualizarBotones();
                controlar();
                resetearControles();
            }
        }
    }

    /// <summary>
    /// Procesamos las ordenes del jugador
    /// </summary>
    private void controlar()
    {
        
        if (left)
        {
            animator.SetInteger("Estado", 1);
            rigidBody.velocity = new Vector2(-3, rigidBody.velocity.y);
            if (haciaDerecha)
            {
                cambiarRotacion();
            }
        }
        else if (right)
        {
            animator.SetInteger("Estado", 1);
            rigidBody.velocity = new Vector2(3, rigidBody.velocity.y);
            if (!haciaDerecha)
            {
                cambiarRotacion();
            }
        }

        if (up && rigidBody.velocity.y == 0)
        {
            animator.SetInteger("Estado", 2);
            if (colisionaJugador)
            {
                rigidBody.velocity = new Vector2(rigidBody.velocity.x, 12f);
            }
            else
            {
                rigidBody.velocity = new Vector2(rigidBody.velocity.x, 8.5f);
            }
            this.colisionaJugador = false;

        }

        if (space)
        {
            animator.SetInteger("Estado", 3);
        }

        if(!left && !right && !up && !space)
        {
            animator.SetInteger("Estado", 0);
        }
    }

    /// <summary>
    /// Realiza el procesamiento de los inputs
    /// </summary>
    private void actualizarBotones()
    {
        space = Input.GetKey(KeyCode.Space);
        left = Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A);
        right = Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D);
        up = Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W);
        //down = Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S);
    }

    /// <summary>
    /// Cambia la rotacion del personaje
    /// </summary>
    public void cambiarRotacion()
    {
        this.transform.eulerAngles = new Vector3(
            this.transform.eulerAngles.x,
            this.transform.eulerAngles.y + 180,
            this.transform.eulerAngles.z
        );
        haciaDerecha = !haciaDerecha;
    }

    /// <summary>
    /// Detecta la colision de bala y quita vida
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name.ToLower().Contains("bala"))
        {
            quitarVida(Random.Range(1, 40));
        }else if (collision.gameObject.name.ToLower().Contains("itemvida"))
        {
            agregarVida(20);
        }
        else if (collision.gameObject.name.ToLower().Contains("itemescudo"))
        {
            agregarEscudo(50);
        }

        if (isIA && collision.gameObject.name.ToLower().Contains("limite"))
        {
            this.colisionaLimite = true;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (isIA && collision.gameObject.name.ToLower().Contains("limite"))
        {
            this.colisionaLimite = true;
        }else if (collision.gameObject.name.ToLower().Contains("p"))
        {
            this.colisionaJugador = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (isIA && collision.gameObject.name.ToLower().Contains("limite"))
        {
            this.colisionaLimite = false;
        }
        else if (collision.gameObject.name.ToLower().Contains("p"))
        {
            this.colisionaJugador = false;
        }
    }

    /// <summary>
    /// Quita vida igual al valor pasado, si queda escudo resta cantidad del escudo
    /// </summary>
    /// <param name="cantidad"></param>
    private void quitarVida(int cantidad)
    {
        if (escudo > 0)
        {
            escudo = escudo - cantidad <= 0 ? 0 : escudo - cantidad;
            activarEscudo();
        }
        else if (vida > 0)
        {
            vida = vida - cantidad <= 0 ? 0 : vida - cantidad;
        }

        if(vida == 0)
        {
            EventosJuego.perder();
        }
    }

    /// <summary>
    /// Activa el escudo
    /// </summary>
    private void activarEscudo()
    {
        escudoObject.SetActive(true);
        Invoke(nameof(desactivarEscudo), 0.250f);
    }

    /// <summary>
    /// Desactiva el escudo
    /// </summary>
    private void desactivarEscudo()
    {
        escudoObject.SetActive(false);
    }

    /// <summary>
    /// Funcion preparada para el evento de disparar, crea una bala en la direccion que apunte el jugador
    /// </summary>
    public void disparar()
    {
        GameObject bala = Instantiate(this.bala, new Vector3(haciaDerecha ? this.transform.position.x+1: this.transform.position.x-1,this.transform.position.y,this.transform.position.z), this.transform.rotation);
        bala.GetComponent<Bala>().creador = this.gameObject.name;
        bala.GetComponent<Bala>().irHaciaDerecha = this.haciaDerecha;
    }

    /// <summary>
    /// Crea una traza en la direccion que apunte el jugador
    /// </summary>
    public void trazar()
    {
        GameObject bala = Instantiate(this.traza, new Vector3(haciaDerecha ? this.transform.position.x + 1 : this.transform.position.x - 1, this.transform.position.y, this.transform.position.z), this.transform.rotation);
        bala.GetComponent<Bala>().creador = this.gameObject.name;
        bala.GetComponent<Bala>().irHaciaDerecha = this.haciaDerecha;
    }

    /// <summary>
    /// Actualiza sus marcadores de nombre,vida,escudo
    /// </summary>
    private void actualizarMarcadores()
    {
        nombreUI.text = this.nombre;
        vidaUI.text = this.vida + "%";
        escudoUI.text = this.escudo + "%";

        marcadores.transform.position = new Vector3(this.transform.position.x,this.transform.position.y+0.2f,this.transform.position.z);
    }

    /// <summary>
    /// Agrega escudo al personaje
    /// </summary>
    /// <param name="cantidad"></param>
    private void agregarEscudo(int cantidad)
    {
        activarEscudo();
        if (this.escudo < 100)
        {
            this.escudo = this.escudo + cantidad >= 100 ? 100 : this.escudo + cantidad;
        }
    }

    /// <summary>
    /// Agrega vida al personaje
    /// </summary>
    /// <param name="cantidad"></param>
    private void agregarVida(int cantidad)
    {
        if (this.escudo < 100)
        {
            this.vida = this.vida + cantidad >= 100 ? 100 : this.vida + cantidad;
        }
    }

    /// <summary>
    /// Limpia los controles
    /// </summary>
    private void resetearControles()
    {
        left = false;
        right = false;
        down = false;
        up = false;
        space = false;
    }
}
