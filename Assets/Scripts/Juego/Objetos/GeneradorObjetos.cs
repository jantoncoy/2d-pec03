using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// Contiene los eventos del generador de objetos
/// </summary>
public static class EventosGeneradorObjetos
{
    /// <summary>
    /// Para el generador de objetos
    /// </summary>
    public static Action pararGenerador;
}

/// <summary>
/// Clase que genera objetos en toda su longitud en X 
/// </summary>
public class GeneradorObjetos : MonoBehaviour
{
    /// <summary>
	/// Indica cada cuantos segundos se genera un objeto
	/// </summary>
    public int intervaloGeneracion = 15;

    /// <summary>
	/// Longitud hacia la derecha
	/// </summary>
    public float longitudXMin = 0;

    /// <summary>
    /// Longitud hacia la derecha
    /// </summary>
    public float longitudXMax = 10;

    /// <summary>
    /// Objeto vida para generar
    /// </summary>
    public GameObject vida;

    /// <summary>
    /// Objeto escudo para generar
    /// </summary>
    public GameObject escudo;

    // Start is called before the first frame update
    void Start()
    {
        //iniciamos la rutina para crear objetos
        InvokeRepeating(nameof(generarObjeto), 0,intervaloGeneracion);
        //Instanciamos evento
        EventosGeneradorObjetos.pararGenerador = pararGeneradorObjetos;
    }

    /// <summary>
	/// Genera un objeto de vida o escudo de forma aleatoria en una posicion aleatoria
	/// </summary>
    private void generarObjeto()
    {
        int numeroRandom = Random.Range(1, 3);
        float posicionRandom = Random.Range(longitudXMin, longitudXMax);

        if (numeroRandom == 1) {
            Instantiate(vida, new Vector3(posicionRandom, this.transform.position.y, 0),this.transform.rotation);
        }
        else
        {
            Instantiate(escudo, new Vector3(posicionRandom, this.transform.position.y, 0), this.transform.rotation);
        }

    }

    /// <summary>
    /// Para el generador de objetos
    /// </summary>
    public void pararGeneradorObjetos()
    {
        CancelInvoke(nameof(generarObjeto));
    }
}
