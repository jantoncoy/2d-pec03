using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase que realiza la funcionalidad de destruir el sonido despues de unos segundos
/// </summary>
public class SonidoExplosion : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke(nameof(destruirme), 2);
    }

    /// <summary>
    /// Se destruye el gameobject con este script
    /// </summary>
    void destruirme()
    {
        Destroy(this.gameObject);
    }
}
