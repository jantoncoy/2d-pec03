using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase con las funcionalidades para las balas
/// </summary>
public class Bala : MonoBehaviour
{
    /// <summary>
    /// Prefab de la explosion
    /// </summary>
    public GameObject explosion,explosionSonido,sonidoDisparo;

    /// <summary>
    /// Indica si se tiene que mover hacia la derecha
    /// </summary>
    public bool irHaciaDerecha = true;

    /// <summary>
    /// Fisicas de la bala
    /// </summary>
    public Rigidbody2D rigidBody = null;

    /// <summary>
    /// Indica si ha colisionado ya
    /// </summary>
    private bool explotada = false;

    /// <summary>
    /// Quien creo la bala
    /// </summary>
    public string creador = "";

    /// <summary>
    /// Indica si es una traza
    /// </summary>
    public bool traza = false;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = this.GetComponent<Rigidbody2D>();
        if (!traza)
        {
            EventosJuego.restarTiempo(2);
            Instantiate(sonidoDisparo, new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), this.gameObject.transform.rotation);
        }
        else
        {
            //InvokeRepeating(nameof(visualizar),0,0.050f);
            //InvokeRepeating(nameof(ocultarBala),0,0.100f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!explotada)
        {
            if (irHaciaDerecha)
            {
                rigidBody.velocity = new Vector2(8, rigidBody.velocity.y);
            }
            else
            {
                rigidBody.velocity = new Vector2(-8, rigidBody.velocity.y);
            }
        }
    }

    /// <summary>
    /// Oculta la bala mientras explota
    /// </summary>
    private void ocultar()
    {
        CancelInvoke();
        ocultarBala();
        Invoke(nameof(destruir), 0.250f);
    }

    /// <summary>
    /// Genera una explosion en el lugar de la bala
    /// </summary>
    private void generarExplosion()
    {
        if (!traza)
        {
            Instantiate(explosionSonido, new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), this.gameObject.transform.rotation);
            Instantiate(explosion, new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), this.gameObject.transform.rotation);
        }
    }

    /// <summary>
    /// Detecta las colisiones para explotar
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.gameObject.name.ToLower().Contains(creador.ToLower()))
        {
            if (!explotada && !traza)
            {
                explotada = true;
                ocultar();
                generarExplosion();
            }

            if (traza)
            {
                ocultar();
            }
        }
    }

    /// <summary>
    /// Destruye esta bala
    /// </summary>
    private void destruir()
    {
        Destroy(this.gameObject);
    }

    /// <summary>
    /// Visualiza la bala con tonalidad roja
    /// </summary>
    private void visualizar()
    {
        this.GetComponent<SpriteRenderer>().color = new Color(255, 0, 0, 255);
    }

    /// <summary>
    /// Oculta la bala
    /// </summary>
    private void ocultarBala()
    {
        this.GetComponent<Renderer>().material.color = new Color(0, 0, 0, 0);
    }
}
