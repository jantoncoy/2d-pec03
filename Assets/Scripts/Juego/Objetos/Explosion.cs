using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contiene las funcionalidades para el prefab de la explosion
/// </summary>
public class Explosion : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// Se encarga de restar vida y/o destruir parte del mapa
    /// </summary>
    public void destruir()
    {
        //llamamos evento si estamos en colision con algun jugador
        //llamamos evento destruir zona
    }

    /// <summary>
    /// Se encarga de destruir el objeto
    /// </summary>
    public void mitigar()
    {
        Destroy(this.gameObject);
    }
}
