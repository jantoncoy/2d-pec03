using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contiene las funciones para los objetos consumibles
/// </summary>
public class Consumible : MonoBehaviour
{
    public GameObject consumible;

    /// <summary>
    /// Cuando detecta colision con un jugador se destruye el objeto
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name.ToLower().Contains("p")
            || collision.gameObject.name.ToLower().Contains("p"))
        {
            Destroy(consumible);
        }
    }
}
