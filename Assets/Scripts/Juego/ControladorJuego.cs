using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Contiene los eventos del juego generales
/// </summary>
public static class EventosJuego
{
    /// <summary>
    /// Permite llamar desde cualquier parte a perder partida
    /// </summary>
    public static Action perder;

    /// <summary>
    /// Resta tiempo al contador
    /// </summary>
    public static Action<int> restarTiempo;
}

/// <summary>
/// Contiene las utilidades para IA 
/// </summary>
public static class EventosIA
{
    /// <summary>
    /// obtiene los jugadores menos el pasado por parametro
    /// </summary>
    public static Func<GameObject, List<GameObject>> obtenerJugadores;
}

/// <summary>
/// Contiene la logica del juego para el funcionamiento de los turnos
/// </summary>
public class ControladorJuego : MonoBehaviour
{
    /// <summary>
    /// Tiempo del jugador por turno en segundos
    /// </summary>
    public int tiempoTurno = 10;

    /// <summary>
    /// Tiempo maximo de la partida en segundos
    /// 5 minutos
    /// </summary>
    public int tiempoPartida = 300;

    /// <summary>
    /// Acumulador del tiempo de juego
    /// </summary>
    public int tiempoPartidaRestante = 0;

    /// <summary>
    /// Acumulador del tiempo de turno
    /// </summary>
    public int tiempoTurnoRestante = 0;

    /// <summary>
    /// Marcador tiempo de turno
    /// </summary>
    public UnityEngine.UI.Text time;

    /// <summary>
    /// Marcador del tiempo de la partida restante
    /// </summary>
    public UnityEngine.UI.Text timetotal;

    /// <summary>
    /// Array de jugadores
    /// </summary>
    public GameObject[] jugadores;

    /// <summary>
    /// Indica el jugador actual
    /// </summary>
    public int jugadorActual = 0;

    /// <summary>
    /// Prefabs de los jugadores
    /// </summary>
    public GameObject jugador1,jugador2;

    /// <summary>
    /// Contiene la camara del juego
    /// </summary>
    public GameObject camara;

    /// <summary>
	/// Contiene el canvas de la pantalla final
	/// </summary>
    public GameObject pantallaFinal;

    /// <summary>
	/// Contiene el texto que indica quien ha ganado
	/// </summary>
    public TextMeshProUGUI textoFinal;

    /// <summary>
    /// Contiene el canvas de los UI
    /// </summary>
    public GameObject pantallaUI;

    // Start is called before the first frame update
    void Start()
    {
        ///lanzamos corrutina tiempo de partida
        tiempoPartidaRestante = 300;
        InvokeRepeating(nameof(restarTiempoJuego), 0, 1.0f);
        InvokeRepeating(nameof(restarTiempoTurno), 0, 1.0f);

        if(OpcionesJuego.numeroJugadores == 1)
        {
            iniciar1P();
        }
        else
        {
            iniciar2P();
        }

        EventosJuego.perder = terminarJuego;
        EventosJuego.restarTiempo = restarTiempo;
        EventosIA.obtenerJugadores = obtenerJugadores;
    }

    /// <summary>
    /// inicia jugadores modo 1 jugador
    /// </summary>
    private void iniciar1P()
    {
        jugadores = new GameObject[2];
        jugadores[0] = Instantiate(jugador1);
        jugadores[0].GetComponent<Controlador>().isIA = false;
        jugadores[1] = Instantiate(jugador2);
        jugadores[1].transform.position = new Vector3(jugadores[1].transform.position.x + 5, jugadores[1].transform.position.y, jugadores[1].transform.position.z);
        jugadores[1].GetComponent<Controlador>().isIA = true;
        jugadores[1].GetComponent<Controlador>().nombre = "Jugador (PC)";
    }

    /// <summary>
    /// inicia jugadores modo 2 jugadores
    /// </summary>
    private void iniciar2P()
    {
        jugadores = new GameObject[2];
        jugadores[0] = Instantiate(jugador1);
        jugadores[0].GetComponent<Controlador>().isIA = false;
        jugadores[0].GetComponent<Controlador>().nombre = "Jugador 1";
        jugadores[1] = Instantiate(jugador2);
        jugadores[1].transform.position = new Vector3(jugadores[1].transform.position.x + 5, jugadores[1].transform.position.y, jugadores[1].transform.position.z);
        jugadores[1].GetComponent<Controlador>().isIA = false;
        jugadores[1].GetComponent<Controlador>().nombre = "Jugador 2";
    }

    // Update is called once per frame
    void Update()
    {
        if(tiempoPartidaRestante == 0)
        {
            //finalizamos el juego
            terminarJuego();
        }else if(tiempoTurnoRestante == 0)
        {
            //cambiamos de jugador
            cambiarTurno();
        }
        actualizarMarcadores();
        vincularCamara();
    }

    /// <summary>
    /// Restamos al tiempo total un segundo
    /// </summary>
    private void restarTiempoJuego()
    {
        if(tiempoPartidaRestante != 0)
        tiempoPartidaRestante = tiempoPartidaRestante - 1;
    }

    /// <summary>
    /// Restamos al tiempo del turno un segundo
    /// </summary>
    private void restarTiempoTurno()
    {
        if(tiempoTurnoRestante != 0)
        tiempoTurnoRestante = tiempoTurnoRestante - 1;
    }

    /// <summary>
    /// Actualizamos el valor de los marcadores (jugador, vida, tiempos)
    /// </summary>
    private void actualizarMarcadores()
    {
        time.text = tiempoTurnoRestante + "s";
        timetotal.text = tiempoPartidaRestante + "s";
    }

    /// <summary>
    /// Realizamos las peticiones necesarias para activar el siguiente jugador
    /// </summary>
    public void cambiarTurno()
    {
        //Al anterior jugador lo inhabilitamos
        jugadores[jugadorActual].GetComponent<Animator>().SetInteger("Estado", 0);
        jugadores[jugadorActual].GetComponent<Controlador>().habilitado = false;

        //cambiamos de jugador
        if (jugadores.Length > (jugadorActual+1))
        {
            jugadorActual++;
        }
        else
        {
            jugadorActual = 0;
        }

        //habilitamos al jugador y reiniciamos el tiempo
        jugadores[jugadorActual].GetComponent<Controlador>().habilitado = true;
        tiempoTurnoRestante = tiempoTurno;
    }

    /// <summary>
    /// Vincula la camara del juego al personaje que tenga el turno
    /// </summary>
    private void vincularCamara()
    {
        camara.transform.position = new Vector3(jugadores[jugadorActual].transform.position.x, jugadores[jugadorActual].transform.position.y-1,camara.transform.position.z);
    }

    /// <summary>
    /// Evaluamos si uno de los jugadores a perdido o quien tiene menos vida
    /// </summary>
    public void terminarJuego()
    {
        //Eliminamos las rutinas
        CancelInvoke();

        GameObject jugadorConMasVida = null;

        //Vamos a buscar el jugador con mas vida, este sera quien gane
        for(int a=0; a < jugadores.Length; a++)
        {
            jugadores[a].GetComponent<Controlador>().habilitado = false;

            int totalJugador = 0;

            if (jugadorConMasVida != null)
            totalJugador = jugadorConMasVida.GetComponent<Controlador>().vida+jugadorConMasVida.GetComponent<Controlador>().escudo;

            int totalJugador2 = jugadores[a].GetComponent<Controlador>().vida + jugadores[a].GetComponent<Controlador>().escudo;

            if (jugadorConMasVida == null || totalJugador < totalJugador2)
            {
                jugadorConMasVida = jugadores[a];
            }
        }

        //Desactivamos UI y activamos pantalla final
        pantallaUI.SetActive(false);
        pantallaFinal.SetActive(true);
        textoFinal.text = "Ha ganado: " + jugadorConMasVida.GetComponent<Controlador>().nombre;
    }

    /// <summary>
    /// Resta segundos al contador de turno
    /// </summary>
    /// <param name="tiempo"></param>
    public void restarTiempo(int tiempo)
    {
        tiempoTurnoRestante = tiempoTurnoRestante - tiempo >= 0 ? tiempoTurnoRestante - tiempo : 0;
    }

    /// <summary>
    /// Devuelve un listado de jugadores, excluye al pasado por parametro
    /// </summary>
    /// <param name="jugador"></param>
    /// <returns></returns>
    public List<GameObject> obtenerJugadores(GameObject jugador)
    {
        List<GameObject> resultado = new List<GameObject>();

        foreach(GameObject player in jugadores)
        {
            if(player != jugador)
            {
                resultado.Add(player);
            }
        }

        return resultado;
    }

}